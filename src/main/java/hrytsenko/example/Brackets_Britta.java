package hrytsenko.example;

public final class Brackets_Britta {

    private Brackets_Britta() {
    }

    /**
     * Check that following brackets are paired: (), [], {}.
     * 
     * @param text
     *            the input text.
     * 
     * @return <code>true</code> if the input text is valid and <code>false</code> otherwise.
     */
    public static boolean validate(String text) 
    {   	
    	System.out.println("\n------------- Analyze String : " + text +  " ----------------");

    	if(text == null)
    	{
    		System.out.println("String is NULL.");
    		System.out.println("  => UnsupportedOperationException ???? ");
    		//throw new UnsupportedOperationException();  /// ??????
    	}

    	if(text.isEmpty())
    	{
    		System.out.println("String is empty.");
    		System.out.println("  => validation was successful ");
    		return true;
    	}
    	
    	// remove whitespace 
    	text = text.trim();
    	// remove all letters except brackets
    	String  onlyBracketStr = extractBrackets(text);
    	System.out.println("     reduced String : " + onlyBracketStr);
    	
    	// test if the length of the String containing brackets only, is not even
    	if (onlyBracketStr.length()%2 == 1)
    	{
    		System.out.println("String is not even");
    		System.out.println("  => validation was not successful ");    		
     		return false;
     	}    	
    
    	Boolean ret = matchBrackets(onlyBracketStr);
    	if (ret)
    	{
    		System.out.println("  => validation was successful ");
    	} else {
    		System.out.println("  => validation was not successful ");
    	}
   	
    	return ret;
    }
    
    // Attention: matchBrackets contains recursion
    private static boolean matchBrackets(String text)
    {
//    	int len = text.length();
    	
    	if (text.isEmpty()) 
    	{   // stop recursion -> nothing more to analyze
    		System.out.println("String is empty after recursion, i.e. it matches, recurssion finished!");
    		return true;
    	}
    	
    	Boolean found = false;    	
		for (int i=1; i < text.length() ; i++)
		{	// compare letter [i] with the preceding letter [i-1] to find a match
			char p = text.charAt(i-1);
			char c = text.charAt(i);

			if ( ( p == '(' && c == ')' ) ||
			     ( p == '{' && c == '}' ) || 
			     ( p == '[' && c == ']' ) )
			{
				found = true; 
				if (i == 1){
					text = text.substring(i+1);

				} else {    // because : substring(beginIndex, endIndex) =>The substring begins at the specified beginIndex and extends to the character at index endIndex - 1
					text = text.substring(0,i-1) + text.substring(i+1);		
				}
		    	System.out.println(".... recursion with text: " + text);
		    	// recursion
				return matchBrackets(text);
			}  
			// if one matching bracket is removed skip the rest of the loop 
			//if (found) break;
		}
    	
    	// nothing found, also after recursion not found
		if (!found)
		{
			return false;
		} else {
			System.out.println(".... !!!!!! Error !!!!!!");
			return true;
		}
/*
    	if ( len%2 == 0)
     	{	// end recursion -> an even string can be matched successfully
    		System.out.println("String gerade");
     		return true;
     	} else 
     	{	// end recursion -> an uneven string can not be matched successfully
    		System.out.println("String ungerade");
     		return false;
     	}
*/  
    	/*
		for (int i=0; i < text.length() ; i++)
		{
			char c = text.charAt(i);
			if ( c == '(' || c == ')' ||
			     c == '{' || c == '}' || 
			     c == '[' || c == ']' )
			{
				text += c; 
			}  		
		}
    	 */		

    }

    private static String extractBrackets(String inStr)
    {
    	String  retStr = "";
    	for (int i=0; i < inStr.length() ; i++)
    	{
    		char c = inStr.charAt(i);
    		if ( c == '(' || c == ')' ||
    		     c == '{' || c == '}' || 
    		     c == '[' || c == ']' )
    		{
    			retStr += c; 
    		}  		
    	}
    	return retStr;
    }
}
