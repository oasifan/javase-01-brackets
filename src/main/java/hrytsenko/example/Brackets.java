package hrytsenko.example;

public final class Brackets {

    private Brackets() {
    }

    /**
     * Check that following brackets are paired: (), [], {}.
     * 
     * @param text
     *            the input text.
     * 
     * @return <code>true</code> if the input text is valid and <code>false</code> otherwise.
     */
    public static boolean validate(String text) {
        throw new UnsupportedOperationException();
    }

}
