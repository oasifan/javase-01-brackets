package hrytsenko.example;

import org.junit.Assert;
import org.junit.Test;

public class BracketsTest_Britta {

	// task: Add parameterized tests.
	private void validateBracketsTestBritta(String text, Boolean success) {
        boolean valid = Brackets_Britta.validate(text);
        if (success)
        {
        	Assert.assertTrue(valid);
        } else {
            Assert.assertFalse(valid);
        }
	}
	
	@Test
	public void validateMyTest_1() {
		validateBracketsTestBritta("(aaa(bbb(ccc)bbb[ddd{eee}ddd]bbb)aaa)", true);
	}
	
	@Test
	public void validateMyTest_2() {
		validateBracketsTestBritta("(ccc[ddd][eee]{bbb(aaa[]aaa)bbb}ccc)", true);
	}
	
	@Test
	public void validateMyTest_3() {
		validateBracketsTestBritta("(ccc[ddd][eee]{bbb(aaa[]aaa}bbb)ccc)", false);
	}
	
	@Test
	public void validateMyTest_4() {
		validateBracketsTestBritta("([d]{ee]{bbb(aaa[]aaa}bbb)ccc)", false);
	}
	
	@Test
	public void validateMyTest_5() {
		validateBracketsTestBritta(")[d]{ee]{bbb(aaa[]aaa)bbb})", false);
	}
	
    @Test(expected = NullPointerException.class)
    public void validate_nullString_throwException() {
        Brackets_Britta.validate(null);
    }

    @Test
    public void validate_emptyString_valid() {
        boolean valid = Brackets_Britta.validate("");
        Assert.assertTrue(valid);
    }

    @Test
    public void validate_noBrackets_valid() {
        boolean valid = Brackets_Britta.validate("foo");
        Assert.assertTrue(valid);
    }

    @Test
    public void validate_roundBrackets_valid() {
        boolean valid = Brackets_Britta.validate("(foo)");
        Assert.assertTrue(valid);
    }

    @Test
    public void validate_squareBrackets_valid() {
        boolean valid = Brackets_Britta.validate("[foo]");
        Assert.assertTrue(valid);
    }

    @Test
    public void validate_curlyBrackets_valid() {
        boolean valid = Brackets_Britta.validate("{foo}");
        Assert.assertTrue(valid);
    }

    @Test
    public void validate_nestedBrackets_valid() {
        boolean valid = Brackets_Britta.validate("(foo(bar))");
        Assert.assertTrue(valid);
    }

    @Test
    public void validate_severalBrackets_valid() {
        boolean valid = Brackets_Britta.validate("(foo){bar}");
        Assert.assertTrue(valid);
    }

    @Test
    public void validate_noClosingBracket_invalid() {
        boolean valid = Brackets_Britta.validate("(foo");
        Assert.assertFalse(valid);
    }

    @Test
    public void validate_noOpeningBracket_invalid() {
        boolean valid = Brackets_Britta.validate("foo)");
        Assert.assertFalse(valid);
    }

    @Test
    public void validate_bracketsNotMatch_invalid() {
        boolean valid = Brackets_Britta.validate("(foo]");
        Assert.assertFalse(valid);
    }

}
